//var tracer = require('./call_tracer.js');

const fs = require('fs');
const strip = require('strip-comments');

var tracerFunc = fs.readFileSync('./call_tracer.js', 'utf-8')


tracerFunc= strip(tracerFunc);

var Web3 = require('web3');

var web3 = new Web3("http://localhost:8888");

var blocknumber = 0;

const promisify = (inner) =>
  new Promise((resolve, reject) =>

    inner((err, res) => {
      console.log(res);
      if (err) { reject(err) }

      resolve(res);
    })
  );

  function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}

async function main(){
	while(true){
  		const block = await web3.eth.getBlock(blocknumber);
  		//console.log(block);

  		block.transactions.map(function(tx){
  			web3.currentProvider.send({
			  jsonrpc: "2.0", 
			  method: "debug_traceTransaction",
			  //https://ethereum.stackexchange.com/questions/40648/full-node-sync-only-preserve-the-last-128-history-states
			  params: [tx,{tracer:tracerFunc,reexec:6000000}], 
			  id: new Date().getTime() // Id of the request; anything works, really
			}, function(err,res) {
			   //res.result.structLogs.map(x=>console.log(x.op)) 
			   console.log(JSON.stringify(res))
			})

  		})
		blocknumber++;
  		await sleep();
	}
};


main();


