/*
* @Author: amitshah
* @Date:   2019-12-08 23:29:17
* @Last Modified by:   amitshah
* @Last Modified time: 2019-12-19 01:45:59
*/



const TracerToken = artifacts.require("TracerToken");
const TracerERC721 = artifacts.require("TracerERC721");
const DistributeERC20 = artifacts.require("DistributeERC20");
 
const fs = require('fs');
const strip = require('strip-comments');
var tracerFunc = fs.readFileSync('../call_tracer.js', 'utf-8')
tracerFunc= strip(tracerFunc);



AbiLut ={};
function loadAbi(){
  Array.prototype.slice.call(arguments).map(c=>{
    AbiLut[c.contractName] = {};  
    c.abi.map(a=>{
      AbiLut[c.contractName][a.name] = a; 
    })
 
  })
}

loadAbi(TracerToken, TracerERC721,DistributeERC20);

//Helper Functions to Assert Trace Collected correctly
function assertParams(obj, params){
  Object.keys(params).forEach(function(key) {
   assert.equal(obj[key], params[key]);
  });
}

function assertCall(call,_from,_to,_value ){
  assertParams(call, {"from":_from, "to":_to, "value":_value});
}


function assertLog(contract,event, input, expectedValues){
  
  let decodedValues = web3.eth.abi.decodeLog(AbiLut[contract][event].inputs, input.data, input.topics.slice(1,));
  
  Object.keys(expectedValues).map(k=>{
  	  assert.equal(k in decodedValues, true, 'invalid key in expected value:'+k);
  	  assert.equal(decodedValues[k].toString().toLowerCase(),expectedValues[k],"log not parsed correctly for key:"+k);
  	
   })

}

function getTrace(txHash){
  return new Promise(function(resolve, reject) {
      web3.currentProvider.send({
        jsonrpc: "2.0", 
        method: "debug_traceTransaction",
        params: [txHash,{tracer:tracerFunc, reexec:500}], // 60 seconds, may need to be hex, I forget
        id: new Date().getTime() // Id of the request; anything works, really
      },function(err,res){
        if(!err){
          resolve(res);
        }
      });
    });
}


function getBalanceFromStorage(tokenAddress,address){
	var key = getStorageKey(address,"0")//position 0
	return web3.eth.getStorageAt(tokenAddress,key);
}
function getStorageKey(address,pos){
	return  web3.utils.sha3(web3.utils.padLeft(address,64)+web3.utils.padLeft(pos,64),{encoding:"hex"})
}

function getPrimitiveStorageKey(pos){
	return web3.utils.sha3(web3.utils.padLeft(pos,64));
}

contract("TEST_ERC_STANDARDS", async accounts => {


  it("should correctly track ERC20 creation and minting", async () =>{
	//identified amit.shah2/ethtracer#2	
  	let token = await TracerToken.new();

  	let trace = (await getTrace(token.transactionHash)).result;

	console.log(JSON.stringify(trace));
	assert.equal(true,false, "tracer cant handle steps in constructor");
	
  });
  


  it("tracer should identify storage updates AND logs in loops", async()=>{
    
    let token = await TracerToken.new();

    let distribution = await DistributeERC20.new();
    pseudoAccounts = [];
    benefits = [];
  	
  	//create 4 pseudo accounts and fund them with 1000...4000 tokens
  	Array.apply(null, {length: 4}).map((x,i)=>{
  		var pa = web3.utils.padLeft(web3.utils.randomHex(20),40);
  		pseudoAccounts.push(pa);
  		benefits.push( i*1000+1000);
  		//await distribution.addBeneficiary(pa, i*1000+1000);
  		
  	})
  	
  	//1100 funding
  	let result = await token.fundAccount(
  		distribution.address,
  		Array.apply(null, {length: 4}).map((x,i)=>{return 1000 + i * 1000}).reduce((acc,n)=>{ return acc+n },1000));
  	
  	let distBalance = await token.balanceOf(distribution.address);
  	assert.equal(distBalance, 11000, "distribution balance not set");
  	console.log(token.address);
  	console.log(distribution.address);
  	let storageKey = await getStorageKey(distribution.address,"0");
  	let storageValue = await getBalanceFromStorage(token.address, distribution.address);
	
	//Assert Trace so far
	let trace = (await getTrace(result.tx)).result;
  	
  	assertParams(trace.store[0],{"key":"0x2", "newValue":23345, "oldValue": 12345});
  	assertParams(trace.store[1],{"key":storageKey, "newValue":11000, "oldValue": web3.utils.padLeft("0x",64)});

  	await distribution.addBeneficiaries(pseudoAccounts,benefits);
  	console.log(`token: ${token.address} distrubution:${distribution.address} beneficiaries:${pseudoAccounts}`)
  	result =(await distribution.distribute(token.address, 3));

  	trace = (await getTrace(result.tx)).result;
  	


  	console.log(trace.log);
  	console.log(pseudoAccounts);
  	pseudoAccounts =  pseudoAccounts.reverse();
  	benefits = benefits.reverse();

  	for(var i=0; i < trace.log.length; i++){
  		var l = trace.log[i];
  
  		assertLog("DistributeERC20","Distribute", l, {"_addr":pseudoAccounts[i], "_amount":benefits[i]});
  	}

  	for(var i=0; i < trace.calls.length; i++){
  		var call = trace.calls[i];
  		assertLog("TracerToken","Transfer", call.log[0], {"from":distribution.address.toLowerCase(),"to":pseudoAccounts[i].toLowerCase(), "value":benefits[i].toString()});

  	}
  

  	//Assert all storage and keys and old new values updates as expected
  	var totalBenefitsRolling = 11000;
  	for(var i=0; i < trace.calls.length; i++){
  		var call = trace.calls[i];
  		prevTotalBenefitsRolling = totalBenefitsRolling;
  		totalBenefitsRolling-=benefits[i];
		assertParams(call.store[0],{"key":getStorageKey(distribution.address, "0"), "newValue":totalBenefitsRolling, "oldValue": prevTotalBenefitsRolling});
  		assertParams(call.store[1],{"key":getStorageKey(pseudoAccounts[i], "0"), "newValue":benefits[i], "oldValue": web3.utils.padLeft("0x",64)});

  	}
 

   })


  
});


