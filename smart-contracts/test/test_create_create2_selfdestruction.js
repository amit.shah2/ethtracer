const C = artifacts.require("C");
const D = artifacts.require("D");
const E = artifacts.require("E");
const ThrowE = artifacts.require("ThrowE");
const SelfDestruct = artifacts.require("SelfDesctructionContract");

 
const fs = require('fs');
const strip = require('strip-comments');
var tracerFunc = fs.readFileSync('../call_tracer.js', 'utf-8')
tracerFunc= strip(tracerFunc);



AbiLut ={};
function loadAbi(){
  Array.prototype.slice.call(arguments).map(c=>{
    AbiLut[c.contractName] = {};  
    c.abi.map(a=>{
      AbiLut[c.contractName][a.name] = a; 
    })
 
  })
}

loadAbi(C,D,E,ThrowE,SelfDestruct);

//Helper Functions to Assert Trace Collected correctly
function assertParams(obj, params){
  Object.keys(params).forEach(function(key) {
   assert.equal(obj[key], params[key]);
  });
}

function assertCall(call,_from,_to,_value ){
  assertParams(call, {"from":_from, "to":_to, "value":_value});
}

function assertStore(call){


}

function assertLog(contract,event, input, expectedValues){
  let decodedValues = web3.eth.abi.decodeParameters(AbiLut[contract][event].inputs, input);
  console.log(decodedValues);
  Object.keys(expectedValues).map(k=>{
    assert.equal(decodedValues[k].toLowerCase(),expectedValues[k],"log not parsed correctly for key:"+k);
  })

}

function getTrace(txHash){
  return new Promise(function(resolve, reject) {
      web3.currentProvider.send({
        jsonrpc: "2.0", 
        method: "debug_traceTransaction",
        params: [txHash,{tracer:tracerFunc, reexec:500}], // 60 seconds, may need to be hex, I forget
        id: new Date().getTime() // Id of the request; anything works, really
      },function(err,res){
        if(!err){
          resolve(res);
        }
      });
    });
}

contract("CREATE_CREATE2_SELFDESTRUCT", async accounts => {


  it("trace correctly gathers bytecode executed in create", async () =>{
    
    let selfDestruct = await SelfDestruct.new();
    let trace = (await getTrace(selfDestruct.transactionHash)).result;
    assert.equal(SelfDestruct.bytecode, trace.input, "trace does not get the correct code deployed");

  });

  it("trace correctly shows value transfered on selfdestruct", async () =>{
    
    //ENSURE correct contract execution
    let account2 = await web3.utils.randomHex(20);
   
    let selfDestruct = await SelfDestruct.new();
    let balanceAddress = await web3.eth.getBalance(account2);
    let balanceContract = await web3.eth.getBalance(selfDestruct.address);
    assert.equal(balanceAddress, 0)
    assert.equal(balanceContract, 0);

    //fund the destruction contract  
    let result = await selfDestruct.sendTransaction({from:accounts[0], value:web3.utils.toWei("400", "ether")})

    balanceAddress = await web3.eth.getBalance(account2);
    balanceContract = await web3.eth.getBalance(selfDestruct.address);
    assert.equal(balanceAddress, 0);  
    assert.equal(balanceContract, web3.utils.toWei("400", "ether"))
    

    //destroy the contract 
    result = await selfDestruct.destroyContract(account2, {from:accounts[0]});
    balanceAddress = await web3.eth.getBalance(account2);
    balanceContract = await web3.eth.getBalance(selfDestruct.address);
    var selfDestructAddress = selfDestruct.address.toLowerCase();

    assert.equal(balanceAddress, web3.utils.toWei("400", "ether"))
    assert.equal(balanceContract, 0);    


    //Ensure Correct trace Execution
    let trace = (await getTrace(result.tx)).result;
    console.log(JSON.stringify(trace));
    assertParams(trace.calls[0], {
        "type": "SELFDESTRUCT",
        "value":web3.utils.toWei("400", "ether"),
        "to": account2,
        "from": web3.utils.padLeft(selfDestructAddress),
        });
   
  });


  // it("trace correctly gathers bytecode executed in create2", async () =>{
  //   //TODO: 
  //    assert.equal(true,false, "create 2 not traced correctly");
  // });



  
});


