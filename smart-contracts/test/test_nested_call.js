const C = artifacts.require("C");
const D = artifacts.require("D");
const E = artifacts.require("E");
const ThrowE = artifacts.require("ThrowE");
const SelfDestruct = artifacts.require("SelfDesctructionContract");
const Refund = artifacts.require("Refund");
 
const fs = require('fs');
const strip = require('strip-comments');
var tracerFunc = fs.readFileSync('../call_tracer.js', 'utf-8')
tracerFunc= strip(tracerFunc);



AbiLut ={};
function loadAbi(){
  Array.prototype.slice.call(arguments).map(c=>{
    AbiLut[c.contractName] = {};  
    c.abi.map(a=>{
      AbiLut[c.contractName][a.name] = a; 
    })
 
  })
}

loadAbi(C,D,E,ThrowE,SelfDestruct);

//Helper Functions to Assert Trace Collected correctly
function assertParams(obj, params){
  Object.keys(params).forEach(function(key) {
   assert.equal(obj[key], params[key]);
  });
}

function assertCall(call,_from,_to,_value ){
  assertParams(call, {"from":_from, "to":_to, "value":_value});
}


function assertLog(contract,event, input, expectedValues){
  let decodedValues = web3.eth.abi.decodeParameters(AbiLut[contract][event].inputs, input);
  console.log(decodedValues);
  Object.keys(expectedValues).map(k=>{
    assert.equal(decodedValues[k].toLowerCase(),expectedValues[k],"log not parsed correctly for key:"+k);
  })

}

function getTrace(txHash){
  return new Promise(function(resolve, reject) {
      web3.currentProvider.send({
        jsonrpc: "2.0", 
        method: "debug_traceTransaction",
        params: [txHash,{tracer:tracerFunc, reexec:500}], // 60 seconds, may need to be hex, I forget
        id: new Date().getTime() // Id of the request; anything works, really
      },function(err,res){
        if(!err){
          resolve(res);
        }
      });
    });
}

contract("C", async accounts => {


  it("should pass value through call chain", async () =>{
    let c = await C.new();
    let d = await D.new();
    let e = await E.new();
    let throwE = await ThrowE.new();
    let selfDestruct = await SelfDestruct.new();
    
    let balance = await web3.eth.getBalance(e.address);
    assert.equal(balance,0,"initial balance wasn't 0");
    let balance2 = await web3.eth.getBalance(d.address);
    assert.equal(balance2,0,"initial balance wasn't 0");
    let balance3 = await web3.eth.getBalance(c.address);
    assert.equal(balance3,0,"initial balance wasn't 0");
    

    let result = await c.foo2(d.address, e.address, 666, {from:accounts[0],value:5000000});
    assert.equal(result.receipt.status, true, "transaction was not successful");

    //Ensure Contract works as expected
    balance = await web3.eth.getBalance(e.address);
    assert.equal(balance,5000000/4,"final balance was not 1250000");
    balance2 = await web3.eth.getBalance(d.address);
    assert.equal(balance2,5000000/4,"initial balance wasn't 1250000");

    balance3 = await web3.eth.getBalance(c.address);
    assert.equal(balance3,5000000/2,"initial balance wasn't 2500000");
    
    var value = await web3.eth.getStorageAt(e.address,"0x")
    assert.equal(value, 666, "value was not set in E");


    //Assert Trace
    let trace = (await getTrace(result.tx)).result; 
    
    assertCall(trace, accounts[0].toString().toLowerCase(), c.address.toLowerCase(),web3.utils.toHex(5000000))
    
    assertCall(trace.calls[0], c.address.toLowerCase(), d.address.toLowerCase(),web3.utils.toHex(5000000/2))
    assertLog("D","GotPaid",  trace.calls[0].log[0].data, {"_sender":c.address.toLowerCase(),"_value":5000000/4})

    assertCall(trace.calls[0].calls[0], d.address.toLowerCase(), e.address.toLowerCase(),web3.utils.toHex(5000000/4))
    assertLog("E","GotPaid2",  trace.calls[0].calls[0].log[0].data, {"_sender":d.address.toLowerCase(),"_num":5000000/4})
  });



  it("should pass value through call chain pure call based", async () =>{
    let c = await C.new();
    let d = await D.new();
    let e = await E.new();
    let throwE = await ThrowE.new();
    let selfDestruct = await SelfDestruct.new();
    
    let balance = await web3.eth.getBalance(e.address);
    assert.equal(balance,0,"initial balance wasn't 0");
    let balance2 = await web3.eth.getBalance(d.address);
    assert.equal(balance2,0,"initial balance wasn't 0");
    let balance3 = await web3.eth.getBalance(c.address);
    assert.equal(balance3,0,"initial balance wasn't 0");
    

    let result = await c.foo(d.address, e.address, 666, {from:accounts[0],value:5000000, gas:800000});
    console.log(result.tx);
    console.log(result.logs);
    assert.equal(result.receipt.status, true, "transaction was not successful");

    balance = await web3.eth.getBalance(e.address);
    assert.equal(balance,5000000/4,"final balance was not 1250000");
    balance2 = await web3.eth.getBalance(d.address);
    assert.equal(balance2,5000000/4,"initial balance wasn't 1250000");
    balance3 = await web3.eth.getBalance(c.address);
    assert.equal(balance3,5000000/2,"initial balance wasn't 2500000");

    var value = await web3.eth.getStorageAt(e.address,"0x")
    assert.equal(value, 666, "value was not set in E");
    
    let trace = (await getTrace(result.tx)).result; 
    
    assertCall(trace, accounts[0].toString().toLowerCase(), c.address.toLowerCase(),web3.utils.toHex(5000000))
    
    assertCall(trace.calls[0], c.address.toLowerCase(), d.address.toLowerCase(),web3.utils.toHex(5000000/2))
    assertLog("D","GotPaid",  trace.calls[0].log[0].data, {"_sender":c.address.toLowerCase(),"_value":5000000/4})

    assertCall(trace.calls[0].calls[0], d.address.toLowerCase(), e.address.toLowerCase(),web3.utils.toHex(5000000/4))
    assertLog("E","GotPaid2",  trace.calls[0].calls[0].log[0].data, {"_sender":d.address.toLowerCase(),"_num":5000000/4})

  });



   it("should identify nested error in stack and continue processing", async () =>{
    let c = await C.new();
    let d = await D.new();
    let e = await E.new();
    let throwE = await ThrowE.new();
    
    let balance = await web3.eth.getBalance(e.address);
    assert.equal(balance,0,"initial balance wasn't 0");
    let balance2 = await web3.eth.getBalance(d.address);
    assert.equal(balance2,0,"initial balance wasn't 0");
    let balance3 = await web3.eth.getBalance(c.address);
    assert.equal(balance3,0,"initial balance wasn't 0");
    

    let result = await c.foo(d.address, throwE.address, 666, {from:accounts[0],value:5000000, gas:800000});
    // console.log(result.tx);
    // console.log(result.logs);
    assert.equal(result.receipt.status, true, "transaction was not successful");

    balance = await web3.eth.getBalance(throwE.address);
    assert.equal(balance,0,"final balance was not 1250000");
    balance2 = await web3.eth.getBalance(d.address);
    assert.equal(balance2,5000000/2,"D balance wasn't 2500000");
    balance3 = await web3.eth.getBalance(c.address);
    assert.equal(balance3,5000000/2,"initial balance wasn't 2500000");

    
    let trace = (await getTrace(result.tx)).result; 
    //console.log(JSON.stringify(trace.calls[0].calls[0] ))
    assertCall(trace, accounts[0].toString().toLowerCase(), c.address.toLowerCase(),web3.utils.toHex(5000000))
    
    assertCall(trace.calls[0], c.address.toLowerCase(), d.address.toLowerCase(),web3.utils.toHex(5000000/2))
    assertLog("D","GotPaid",  trace.calls[0].log[0].data, {"_sender":c.address.toLowerCase(),"_value":5000000/4})

    assertCall(trace.calls[0].calls[0], d.address.toLowerCase(), throwE.address.toLowerCase(),web3.utils.toHex(5000000/4))
    assert.equal(trace.calls[0].calls[0].error, "execution reverted","did not receive error with throwE");

    
    assertParams(trace.calls[0].calls[0].store[0],{"key":"0x0", "newValue":666, "oldValue": web3.utils.padLeft("0x",64)});
  });


  it("does not create store entries on static call but allows logs", async () =>{
    //TODO: 
     assert.equal(true,false, "staticall not traced correctly");
  });


  it("determines delegatecall state changes to correct data storage (caller context updated)", async () =>{
    //TODO: 
     assert.equal(true,false, "delegatecall 2 not traced correctly");
  });

  it("multiple value and log updates in single call frame", async () =>{
    //TODO: Use Refund Conctract

    //USE THE REFUND Contract
     assert.equal(true,false, "does not handle multiple store and logs correctly in single call frame");
  });



});


