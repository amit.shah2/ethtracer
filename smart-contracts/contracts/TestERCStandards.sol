pragma solidity ^0.5.0;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721.sol";

contract TracerToken is ERC20 {

	string public name = "TracerToken";
	string public symbol = "TT";
	uint8 public decimals = 2;
	uint public INITIAL_SUPPLY = 12345;

	constructor() public {
	  _mint(msg.sender, INITIAL_SUPPLY);
	}

	function fundAccount(address receiver, uint256 amount) public {
		_mint(receiver, amount );
	}


	

}

contract TracerERC721 is ERC721{
    

    /**
    * Custom accessor to create a unique token
    */
    function mintUniqueTokenTo(
        address _to,
        uint256 _tokenId
    ) public
    {
        _mint(_to, _tokenId);  
    }
}

contract DistributeERC20{
  mapping (address => address) public llIndex;
  mapping (address => uint) public balances;
  uint256 public totalBenefits;
  address[] public beneficiaries;
  event Distribute(address _addr, uint _amount);

  function addBeneficiary(address _addr, uint amount) external 
  {
      require(amount>0);
      if(balances[_addr]==0){
        llIndex[_addr] = llIndex[address(0x0)];
        llIndex[address(0x0)] = _addr;
      }
      balances[_addr] = balances[_addr]+amount;

  }

  function addBeneficiaries (address[] memory _addrs, uint256[] memory _amounts) public returns(bool){
  	require(_addrs.length == _amounts.length);
  	for(uint i=0;i<_addrs.length;i++) {

        if(balances[_addrs[i]]==0){
	        llIndex[_addrs[i]] = llIndex[address(0x0)];
	        llIndex[address(0x0)] = _addrs[i];
     	}
     	balances[_addrs[i]] = balances[_addrs[i]]+_amounts[i];
    	totalBenefits = totalBenefits+_amounts[i];
    	beneficiaries.push(_addrs[i]);
    }
  	
  	 return true;
  }
  function distribute(ERC20 token, uint n) public{
    do{
      address payable addr = address(uint160(llIndex[address(0x0)]));
      if(addr==address(0x0)){
      	break;
      }
      token.transfer(addr, balances[addr]); //Here we want to ignore if refund fails and keep the loot
      llIndex[address(0x0)] = llIndex[addr];//pop the address of circular list
      emit Distribute(addr,balances[addr]);
      //balances[addr]=0;
      n--;      
    }while(n>=0);
    
  }

}