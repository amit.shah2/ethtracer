pragma solidity ^0.5.0;



contract pingPongTransfer{
   function() payable external{      
      msg.sender.transfer(msg.value/2);
  }
}


contract Refund{

  //lets use a circular linked list
  mapping (address => address) public llIndex;
  mapping (address => uint) public balances;
  
  event Refund(address _addr, uint _amount);
  function fund(address _addr) payable external 
  {
      require(msg.value>0);
      if(balances[_addr]==0){
        llIndex[_addr] = llIndex[address(0x0)];
        llIndex[address(0x0)] = _addr;
      }
      balances[_addr] = balances[_addr]+msg.value;

  }


  function refund(uint n) public{
    do{
      address payable addr = address(uint160(llIndex[address(0x0)]));
      addr.send(balances[addr]); //Here we want to ignore if refund fails and keep the loot
      llIndex[address(0x0)] = llIndex[addr];//pop the address of circular list
      emit Refund(addr,balances[addr]);
      balances[addr]=0;
      
      
      n--;
      
    }while(n>0);
    
  }
}


contract D {
  uint public n;
  address public sender;
  event GotPaid(address _sender, uint256 _value);

//   function callAndLoad(address  _e, uint _n) public {
//     bool result = _e.call(abi.enbytes4(keccak256("setN()"))); // E's storage is set, D is not modified 
//   }
 
// in solidity 0.5+
  function callSetN(address payable  _e, uint _n) payable external {
    //address payable e = address(uint160(address(_e)));
    _e.call.value(msg.value/2).gas(600000)(abi.encodeWithSignature("setN(uint256)", _n)); // E's storage is set, D is not modified 
    emit GotPaid(msg.sender, msg.value/2);
  }
  
  function callSetNNP(address  _e, uint _n) public {
    //address payable e = address(uint160(address(_e)));
    n = _n;
    _e.call(abi.encodeWithSignature("setNNP(uint256)", _n)); // E's storage is set, D is not modified 
    emit GotPaid(msg.sender,_n);
  }


}

contract E {
  uint public n;
  address public sender;
  event GotPaid2(address _sender, uint256 _num);
  function setN(uint _n) payable external {
     n = _n;
    //sender = msg.sender;
    // msg.sender is D if invoked by D's callcodeSetN. None of E's storage is updated
    // msg.sender is C if invoked by C.foo(). None of E's storage is updated

    // the value of "this" is D, when invoked by either D's callcodeSetN or C.foo()
    emit GotPaid2(msg.sender, msg.value);
  }
 
  
}

//use this contract to revert the last payable in stack when testing C->D->E.  
//In the call case, C->D value transfer still occurs, so value is stuck in D when E throws
contract ThrowE{
    uint public n;
    address sender; 
    
    function setN(uint _n) payable public{
      n = _n;
      sender = msg.sender;
      revert();
    }
}

contract C {
    //_funcname = ["callAndLoad(address,uint256)","callcodeAndLoad(address,uint256)", "delegatecallAndLoad(address,uint256)"]
    function foo(address payable _d, address payable _e, uint _n) payable public {
       
       //address(_d).call.value(msg.value)(abi.encodeWithSignature(_funcname,_n));
       address(_d).call.gas(180000).value(msg.value/2)(abi.encodeWithSignature("callSetN(address,uint256)",_e,_n));
    }
    
     function foo2(D _d, address payable _e, uint _n) payable public {
       //address(_d).call.value(msg.value)(abi.encodeWithSignature(_funcname,_n));
       _d.callSetN.value(msg.value/2)(_e,_n);
    }
    
     function fooNP(address  _d, address _e, uint _n)  public {
       //address(_d).call.value(msg.value)(abi.encodeWithSignature(_funcname,_n));
       _d.call(abi.encodeWithSignature("callSetNNP(address,uint256)",_e,_n));
    }
}

contract SelfDesctructionContract {
    address payable public owner;
    string public someValue;
   
  modifier ownerRestricted {
      require(owner == msg.sender);
      _;
  } 
  // constructor
  constructor() public {
      owner = msg.sender;
  }
  // a simple setter function
  function setSomeValue(string memory value) public {
      someValue = value;
  } 
   
  function() payable external{
       
  }
   
  // you can call it anything you want
  function destroyContract(address payable send_to)  public {
     selfdestruct(send_to);
  }
} 

// contract CarShop {
//   address[] carAssets;
//   function createChildContract(string brand, string model) public     payable {
//       // insert check if the sent ether is enough to cover the car asset ...
//       address newCarAsset = new CarAsset(brand, model, msg.sender);            
//       carAssets.push(newCarAsset);   
//   }
//   function getDeployedChildContracts() public view returns (address[]) {
//       return carAssets;
//   }
// }
// contract CarAsset {
//   string public brand;
//   string public model;
//   address public owner;
//   function CarAsset(string _brand, string _model, address _owner) public {
//       brand = _brand;
//       model = _model;
//       owner = _owner;
//   }
// }



//TEST CREATE2 FACTORY SEMANTICS
contract Factory {
  event Deployed(address addr, uint256 salt);

  function deploy(bytes memory code, uint256 salt) public {
    address addr;
    assembly {
      addr := create2(0, add(code, 0x20), mload(code), salt)
      if iszero(extcodesize(addr))
      {
        revert(0, 0)
      }
    }

    emit Deployed(addr, salt);
  }
}


contract RevertOnPayAddress {
  function() payable external{
    revert("NO_FUNDS_PLEASE");
  }
}

contract ThrowOnPayAddress {
  function() payable external{
    //this mimics traditional throw semantics and prevents compiler complaints
    assembly{
      invalid()
    }
    
  }
}


