/*
* @Author: amitshah
* @Date:   2019-12-05 22:40:31
* @Last Modified by:   amitshah
* @Last Modified time: 2019-12-08 23:15:15
*/
var C = artifacts.require("C");
var D = artifacts.require("D");

var E = artifacts.require("E");
var SelfDesctructionContract = artifacts.require("SelfDesctructionContract");
var ThrowE = artifacts.require("ThrowE");
var TracerToken = artifacts.require("TracerToken");

module.exports = function(deployer) {
  // deployment steps

 var c,d,e,selfDestruct = null;

 deployer.then(function() {
  // Create a new version of A
  return deployer.deploy(E, {overwrite:true});
}).then(function() {
  
  // Get the deployed instance of B
  return deployer.deploy(D, {overwrite:true});
}).then(function() {
 
  // Set the new instance of A's address on B via B's setA() function.
  return  deployer.deploy(C, {overwrite:true});
}).then(function(){
	return deployer.deploy(SelfDesctructionContract);
	
}).then(()=>{ 
	return deployer.deploy(ThrowE,{overwrite:true})
}).then(()=>{ 
	return deployer.deploy(TracerToken)
});;

};