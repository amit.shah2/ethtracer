
const fs = require('fs');
const strip = require('strip-comments');
const http = require('http');

var tracerFunc = fs.readFileSync('./call_tracer.js', 'utf-8')


tracerFunc= strip(tracerFunc);

var Web3 = require('web3');

const PROVIDER_URI = "http://localhost:8888";
const GRAPHQL_URI = PROVIDER_URI+"/graphql";

var web3 = new Web3(PROVIDER_URI);


// web3.currentProvider.send({
//   jsonrpc: "2.0", 
//   method: "debug_traceTransaction",
//   params: ["0x33ba8174299aef953ce14dcd01f40820005e5cdf2a87228b46049d544b24fdf8",{tracer:tracerFunc, reexec:500}], // 60 seconds, may need to be hex, I forget
//   id: new Date().getTime() // Id of the request; anything works, really
// }, function(err,res) {
//    //res.result.structLogs.map(x=>console.log(x.op)) 
//    console.log(JSON.stringify(res))
// })

const blockNum = BigInt(4893957)

// web3.currentProvider.send({
//   jsonrpc: "2.0", 
//   method: "debug_traceBlockByNumber",
//   params: ["0x"+blockNum.toString(16),{tracer:tracerFunc, reexec:500, timeout:"1000s"}], // 60 seconds, may need to be hex, I forget
//   id: new Date().getTime() // Id of the request; anything works, really
// }, function(err,res) {
//    //res.result.structLogs.map(x=>console.log(x.op)) 
//    console.log(JSON.stringify(res))
// })







function counter(max) {
    let resolve = null;
    const finished = new Promise(resolveInternal => {
        resolve = resolveInternal
    })

    const count = () => {
        if (!--max) resolve()
    }

    return {
        count,
        finished
    }
}



var transactions = [
{"name":"brd_token_transfer", "tx_hash":"0x54e5f8048e6faf3cc78b53b83322fe545340af806617a1ebddfa5c3b58c0565a","url":"https://etherscan.io/tx/0x54e5f8048e6faf3cc78b53b83322fe545340af806617a1ebddfa5c3b58c0565a/advanced"},
 {"name":"brd_token_distribution", "tx_hash":"0xbeab033da48f4c8b6333769fc51b032bb35127bc5600f2ac44829f9bfe5382c9", "url":"https://etherscan.io/tx/0xbeab033da48f4c8b6333769fc51b032bb35127bc5600f2ac44829f9bfe5382c9"},
 {"name":"erc721-cryptokitty-birth-transfer", "tx_hash":"0x34a423498faa78bd5d688486e1bc7106c9047f54014020a3d823410b5f955f47", "url":"https://etherscan.io/tx/0x34a423498faa78bd5d688486e1bc7106c9047f54014020a3d823410b5f955f47"},
 {"name":"random-multi-eth-transfer-contract-execution", "tx_hash":"0xef3bea425a1ea44c505c9dcb089d2475043ae6b31f3d414f4c53a7e6578cec9a", "url":"https://etherscan.io/tx/0xef3bea425a1ea44c505c9dcb089d2475043ae6b31f3d414f4c53a7e6578cec9a"},
 {"name":"out-of-gas-exception", "tx_hash":"0x963d1ad0bec3f95517d427e8cb7cc262f421bc4ae14046f7bfdb695e548d2419", "url":"https://etherscan.io/tx/0x963d1ad0bec3f95517d427e8cb7cc262f421bc4ae14046f7bfdb695e548d2419"},
 {"name":"reverted-tx", "tx_hash":"0xdfa2e3e14da4c0647c7c63a22dac4ec956cab7b8dd8d503896d346cd8c33893a"},
 {"name":"invalid-function-params-invalid-op-code-0xfe-used-for-runtime-exceptions", "tx_hash":"0x0c4e5b72c10fc9ad11abc7a2ff2b57b8eecda2fdcaa94a293b15e76f6b77c2c3"},
 {"name":"plain-eth-transfer-account-to-account", "tx_hash":"0x1654268ea82a40d697518b46cf9da7fa8333ce1dfb0068529ff8fe48f10440d9","url":"https://etherscan.io/tx/0x1654268ea82a40d697518b46cf9da7fa8333ce1dfb0068529ff8fe48f10440d9"},
 {"name":"delegate-call-interal-tx", "tx_hash":"0x89467d2747b5b1f91fc7fa963ef66802455e134d2ea14d6219f2133bb08e701e","url":"https://etherscan.io/tx/0x89467d2747b5b1f91fc7fa963ef66802455e134d2ea14d6219f2133bb08e701e/advanced"},
 {"name":"parity-bug-self-destruct", "tx_hash":"0x47f7cff7a5e671884629c93b368cb18f58a993f4b19c2a53a8662e3f1482f690","url":"https://etherscan.io/tx/0x47f7cff7a5e671884629c93b368cb18f58a993f4b19c2a53a8662e3f1482f690"},
];

const done = counter(transactions.length);


function requestTransactionGraphql(tx_hash){
	return new Promise((resolve,reject)=>{
		var data = "";
			var query = JSON.stringify({"query":"query{ transaction(hash:\""+tx_hash+"\"){ from{ address } to{ address } gas gasUsed gasPrice hash inputData nonce index value status r s v  "+
	"block {  number ommerHash miner{address} hash parent{ hash } nonce transactionsRoot stateRoot receiptsRoot logsBloom extraData difficulty totalDifficulty gasLimit gasUsed timestamp mixHash } }}"});
const req = http.request({host:"206.189.74.129", port:8890, method:"POST", path:"/graphql", headers:{'Content-Type':"application/json", 'Content-Length':query.length}},
	res=>{
		res.on('data', d=>{
			data+=d;
			
		})
		res.on("end", ()=>{
 			resolve(data);
 		})
	});
 	
	req.on("error", (err)=>{
		reject(err);
	})
	req.write(query);
	req.end();


	})

}

// var query = JSON.stringify({"query":"query{ transaction(hash:\"0x54e5f8048e6faf3cc78b53b83322fe545340af806617a1ebddfa5c3b58c0565a\"){ from{ address } to{ address } gas gasUsed gasPrice hash inputData nonce index value status r s v  "+
// 	"block {  number ommerHash miner{address} hash parent{ hash } nonce transactionsRoot stateRoot receiptsRoot logsBloom extraData difficulty totalDifficulty gasLimit gasUsed timestamp mixHash } }}"});
// const req = http.request({host:"206.189.74.129", port:8890, method:"POST", path:"/graphql", headers:{'Content-Type':"application/json", 'Content-Length':query.length}},
// 	res=>{
// 		res.on('data', d=>{
// 			process.stdout.write(d);

// 		})
// 	});
// req.write(query);
// req.end();
async function work(){
	
	var result = [];
	var rmap = {};
	var graphql = [];
	for(var i in transactions){
		var idx = i;
		var tx = transactions[idx];
		var gql = JSON.parse(await requestTransactionGraphql(tx.tx_hash));
		transactions[idx].time =  new Date().getTime() ;
		graphql.push(gql);
		
		web3.currentProvider.send({
		  jsonrpc: "2.0", 
		  method: "debug_traceTransaction",
		  params: [tx.tx_hash,{tracer:tracerFunc, reexec:500, timeout:"1000s"}], // 60 seconds, may need to be hex, I forget
		  id: tx.time// Id of the request; anything works, really
		}, function(err,res) {
		   //res.result.structLogs.map(x=>console.log(x.op)) 
		   rmap[res.id]=res;
		   result.push({...tx,"trace_rpc_result":res});
		   done.count();
		   //console.log(JSON.stringify(res))
		})
	}
	done.finished.then(()=>{
		console.log(JSON.stringify(result));
		for(var i=0; i< transactions.length; i++){
			transactions[i].trace_rpc_result = rmap[transactions[i].time];
			transactions[i].block_hash = (graphql.filter(g=>g.data.transaction.hash === transactions[i].tx_hash)[0]).data.transaction.block.hash;
		}
		fs.writeFileSync("graphql_rpc_result.json", JSON.stringify(graphql))
		fs.writeFileSync('trace_rpc_result.json', JSON.stringify(transactions));
	})
}

work();



//0x08093a81ebecb0ae71e8655590418adc8c40faeadc17045160c170fb3ddf2619

//0xa723c3239b7fc03a2be4a145bc477ed542ae086d9e04a8efd547baebdebabbb9